"""
Test simple creation and deletion of a network_interface
"""
import pprint

import pytest


# Parametrization options for running each test with --test first and then without --test
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, network_interface_name, aws_ec2_subnet, __test):
    """
    Create a vanilla network_interface without using the network_interface fixture
    """
    ret = await hub.states.aws.ec2.network_interface.present(
        ctx,
        name=network_interface_name,
        client_token=network_interface_name,
        subnet_id=aws_ec2_subnet["SubnetId"],
        tags={"Name": network_interface_name},
    )

    assert ret["result"], pprint.pformat(ret["comment"])
    assert ret["new_state"], ret["comment"]
    # Verify the nic in the response is the nic we actually created
    assert ret["new_state"]["tags"]
    assert ret["new_state"]["tags"]["Name"] == network_interface_name

    if ctx.test:
        return

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx,
        filters=[
            {
                "Name": "network-interface-id",
                "Values": [ret["new_state"]["resource_id"]],
            }
        ],
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_present_without_required_parameters(hub, ctx):
    """
    Create a network_interface without specifying a subnet - should fail as subnet_id is required
    """
    nic_without_subnet = "nic-without-subnet"
    with pytest.raises(Exception) as e:
        await hub.states.aws.ec2.network_interface.present(
            ctx,
            name=nic_without_subnet,
            client_token=nic_without_subnet,
        )

    assert "missing a required argument: 'subnet_id'" in str(e)


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(hub, ctx, network_interface_name):
    """
    Verify that "get" is successful after an network_interfaces has been created
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )
    assert get.result, get.comment
    assert get.ret, get.comment

    ret = await hub.exec.aws.ec2.network_interface.get(
        ctx, resource_id=get.ret.resource_id
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment

    # Verify that the network_interface id matches for both
    assert ret.ret["resource_id"] == get.ret["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx, network_interface_name):
    """
    Verify that "list" is successful after an network_interfaces has been created
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )

    ret = await hub.exec.aws.ec2.network_interface.list(
        ctx, filters=[{"Name": "network-interface-id", "Values": [get.ret.resource_id]}]
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment
    # Verify that the created network_interface is in the list
    assert ret.ret[0]["resource_id"] == get.ret.resource_id


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_fixture(hub, ctx, aws_ec2_network_interface, network_interface_name):
    """
    Use the network_interface fixture and verify that it is functional.
    Nothing new should be created by the fixture in this test since both the fixture
    and the network_interface present state tests use the "network_interface_name" module level fixture
    """
    get = await hub.exec.aws.ec2.network_interface.list(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )
    # Verify that the fixture resulted in a usable network_interface

    assert aws_ec2_network_interface["resource_id"] == get.ret[0]["resource_id"]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_describe(hub, ctx, network_interface_name, __test):
    """
    Describe all network_interfaces and run the "present" state the described network_interface created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )

    # Describe all network_interfaces
    ret = await hub.states.aws.ec2.network_interface.describe(ctx)
    assert get.ret.resource_id in ret

    # Run the present state for our resource created by describe
    network_interface_kwargs = {}
    for pair in ret[get.ret.resource_id]["aws.ec2.network_interface.present"]:
        network_interface_kwargs.update(pair)

    # Run the present state on the result of "describe, no changes should be made
    network_interface_ret = await hub.states.aws.ec2.network_interface.present(
        ctx, **network_interface_kwargs
    )

    assert network_interface_ret["result"], network_interface_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert network_interface_ret["old_state"] == network_interface_ret["new_state"]
    assert not network_interface_ret["changes"]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_present_with_invalid_state(hub, ctx, network_interface_name):
    """
    Try to create a network_interface with invalid subnet_id
    """
    ret = await hub.states.aws.ec2.network_interface.present(
        ctx,
        name="nic-with-invalid-subnet_id",
        subnet_id="123",
    )

    assert not ret["result"]
    assert ret["comment"]
    assert "InvalidSubnetID.NotFound" in ret["comment"][0][0]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_absent(hub, ctx, network_interface_name, __test):
    await delete_network_interface(hub, ctx, network_interface_name)


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_present_nic_with_primary_ip(
    hub, ctx, network_interface_with_primary_ip_name, aws_ec2_subnet
):
    """
    Create a network_interface with primary IP
    """
    random_ip = hub.tool.utils.random_host_ip(aws_ec2_subnet["CidrBlock"])
    ret = await hub.states.aws.ec2.network_interface.present(
        ctx,
        name=network_interface_with_primary_ip_name,
        client_token=network_interface_with_primary_ip_name,
        subnet_id=aws_ec2_subnet["SubnetId"],
        tags={"Name": network_interface_with_primary_ip_name},
        primary_ip_address=random_ip,
    )

    assert ret["result"], (
        pprint.pformat(ret["comment"])
        + " ip="
        + random_ip
        + " cidr="
        + aws_ec2_subnet["CidrBlock"]
    )
    assert ret["new_state"], ret["comment"]

    # Verify the nic is created with the expected IP address
    assert ret["new_state"]["primary_ip_address"] == random_ip
    assert ret["new_state"]["private_ip_addresses"]
    assert len(ret["new_state"]["private_ip_addresses"]) == 1
    assert ret["new_state"]["private_ip_addresses"][0] == random_ip


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_absent_nic_with_primary_ip(
    hub, ctx, network_interface_with_primary_ip_name
):
    await delete_network_interface(hub, ctx, network_interface_with_primary_ip_name)


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_present_nic_with_two_ips(
    hub, ctx, network_interface_with_two_ips_name, aws_ec2_subnet
):
    """
    Create a network_interface with primary IP and secondary IP
    """
    primary_ip = hub.tool.utils.random_host_ip(aws_ec2_subnet["CidrBlock"])
    secondary_ip = hub.tool.utils.next_ip(primary_ip)

    ret = await hub.states.aws.ec2.network_interface.present(
        ctx,
        name=network_interface_with_two_ips_name,
        client_token=network_interface_with_two_ips_name,
        subnet_id=aws_ec2_subnet["SubnetId"],
        tags={"Name": network_interface_with_two_ips_name},
        primary_ip_address=primary_ip,
        private_ip_addresses=[primary_ip, secondary_ip],
    )

    assert ret["result"], (
        pprint.pformat(ret["comment"])
        + " primary_ip="
        + primary_ip
        + " secondary_ip="
        + secondary_ip
        + " cidr="
        + aws_ec2_subnet["CidrBlock"]
    )
    assert ret["new_state"], ret["comment"]

    # Verify the nic is created with the expected IP addresses
    assert ret["new_state"]["primary_ip_address"] == primary_ip
    assert ret["new_state"]["private_ip_addresses"]
    assert len(ret["new_state"]["private_ip_addresses"]) == 2
    assert primary_ip in ret["new_state"]["private_ip_addresses"]
    assert secondary_ip in ret["new_state"]["private_ip_addresses"]


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_absent_nic_with_two_ips(hub, ctx, network_interface_with_two_ips_name):
    await delete_network_interface(hub, ctx, network_interface_with_two_ips_name)


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_present_nic_without_primary_ip(hub, ctx, aws_ec2_subnet):
    """
    Create a network_interface with secondary IP and no primary IP
    """
    secondary_ip = hub.tool.utils.random_host_ip(aws_ec2_subnet["CidrBlock"])
    network_interface_name = "nic-without-primary-ip"

    ret = await hub.states.aws.ec2.network_interface.present(
        ctx,
        name=network_interface_name,
        subnet_id=aws_ec2_subnet["SubnetId"],
        private_ip_addresses=[secondary_ip],
    )

    assert not ret["result"], pprint.pformat(ret["comment"])
    assert not ret["new_state"]
    assert (
        "When private_ip_addresses is used, the primary address should also be specified in primary_ip_address"
        in ret["comment"][0]
    )


async def delete_network_interface(hub, ctx, network_interface_name):
    """
    Destroy a network_interface created by the present state
    """
    get = await hub.exec.aws.ec2.network_interface.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [network_interface_name]}]
    )

    if get.result is True and get.ret is not None:
        ret = await hub.states.aws.ec2.network_interface.absent(
            ctx, name=network_interface_name, resource_id=get.ret.resource_id
        )
    else:
        ret = await hub.states.aws.ec2.network_interface.absent(
            ctx, name=network_interface_name, resource_id=None
        )

    assert ret["result"], ret["comment"]
    assert not ret["new_state"]
