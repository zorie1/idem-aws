import copy
import uuid
from collections import ChainMap

import pytest

AWS_Org_Not_In_Use_Exception = "AWSOrganizationsNotInUseException"


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization(hub, ctx):
    name = "idem-test-organization-" + str(uuid.uuid4())

    # Create organization with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    present_ret = await hub.states.aws.organizations.organization.present(
        test_ctx, name=name, feature_set="ALL"
    )
    assert present_ret["result"], present_ret["comment"]
    if hub.tool.utils.is_running_localstack(ctx):
        assert not present_ret.get("old_state")
        assert (
            f"Would create aws.organizations.organization '{name}'."
            in present_ret["comment"]
        )
    else:
        assert (
            f"aws.organizations.organization '{name}' already exists."
            in present_ret["comment"]
        )
    new_state = present_ret.get("new_state")
    assert new_state

    assert "ALL" == new_state["feature_set"]

    # create organization
    present_ret = await hub.states.aws.organizations.organization.present(
        ctx, name=name, feature_set="ALL"
    )

    assert present_ret["result"], present_ret["comment"]
    if hub.tool.utils.is_running_localstack(ctx):
        assert not present_ret.get("old_state")
    new_state = present_ret.get("new_state")
    assert new_state
    existing_or_created_org_id = new_state["resource_id"]
    assert "ALL" == new_state["feature_set"]
    assert "resource_id" in new_state
    assert "arn" in new_state
    assert "master_account_arn" in new_state
    assert "master_account_id" in new_state
    assert "master_account_email" in new_state
    assert "available_policy_types" in new_state
    assert "roots" in new_state

    if (
        f"aws.organizations.organization '{name}' already exists."
        in present_ret["comment"]
    ):
        assert present_ret["result"], present_ret["comment"]
        assert present_ret.get("old_state")

    else:
        assert present_ret["result"], present_ret["comment"]
        assert not present_ret.get("old_state")

    hub.tool.org_test_util.validate_organization(new_state)

    # describe organization
    describe_ret = await hub.states.aws.organizations.organization.describe(ctx)

    # check if described org_id matches with the created org_id
    assert existing_or_created_org_id in describe_ret

    describe_org_response = describe_ret[existing_or_created_org_id].get(
        "aws.organizations.organization.present"
    )

    described_resource_map = dict(ChainMap(*describe_org_response))
    described_resource_map[
        "roots"
    ] = "Sample root for organization created by Idem integration test"

    hub.tool.org_test_util.validate_organization(described_resource_map)

    list_roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    assert list_roots_resp

    # Delete organization with test flag
    delete_ret = await hub.states.aws.organizations.organization.absent(
        test_ctx, name=name
    )
    assert delete_ret["result"], delete_ret["comment"]
    assert delete_ret.get("old_state") and not delete_ret.get("new_state")
    assert (
        f"Would delete aws.organizations.organization '{name}'."
        in delete_ret["comment"]
    )

    # try to delete organization if present
    delete_ret = await hub.states.aws.organizations.organization.absent(ctx, name=name)
    if delete_ret["result"]:
        assert delete_ret["comment"]
        assert delete_ret.get("old_state") and not delete_ret.get("new_state")

        # Deleting organization again should be a no-op
        ret = await hub.states.aws.organizations.organization.absent(ctx, name=name)
        assert ret["result"], ret["comment"]
        assert (not ret.get("old_state")) and (not ret.get("new_state"))
        assert (
            f"aws.organizations.organization '{name}' is already absent"
            in ret["comment"]
        )
    else:
        assert delete_ret["comment"]
        assert any(
            "OrganizationNotEmptyException" in comment
            for comment in delete_ret["comment"]
        )
