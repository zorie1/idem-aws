import copy
import os
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_alias(hub, ctx, aws_kms_key):
    # Get account from environment
    CI_ACCT_NUM = os.getenv("CI_ACCT_NUM")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert (
            CI_ACCT_NUM
        ), "AWS account number has to be supplied as CI_ACCT_NUM to run this test."
    CI_ACCT_NUM = CI_ACCT_NUM if CI_ACCT_NUM else "6767676767"

    print(f"Going to use account '{CI_ACCT_NUM}' for KMS key")

    name = "alias/idem-fixture-alias-" + str(uuid.uuid4())
    target_key_id = aws_kms_key.get("resource_id")

    # Create alias with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.kms.alias.present(
        test_ctx, name=name, target_key_id=target_key_id
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.kms.alias" in str(ret["comment"])
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert name == resource.get("name")
    assert target_key_id == resource.get("target_key_id")

    # Create alias
    ret = await hub.states.aws.kms.alias.present(
        ctx, name=name, target_key_id=target_key_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")

    resource = ret.get("new_state")
    resource_id = resource.get("resource_id", None)
    assert name == resource_id
    assert target_key_id == resource.get("target_key_id")

    # Verify that describe output format is correct
    describe_ret = await hub.states.aws.kms.alias.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.kms.alias.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.kms.alias.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "arn" in described_resource_map and "target_key_id" in described_resource_map
    assert target_key_id == described_resource_map.get("target_key_id")

    # Create a new key to test update
    key_name = "idem-fixture-kms-key-new" + str(uuid.uuid4())
    ret = await hub.states.aws.kms.key.present(
        ctx,
        key_name,
        policy='{ "Version" : "2012-10-17",  "Id" : "key-consolepolicy-3", '
        '"Statement" : [{"Sid" : "Enable IAM User Permissions","Effect" : "Allow", '
        '"Principal" : {"AWS" : "arn:aws:iam::'
        f"{CI_ACCT_NUM}"
        ':root"},"Action" : [ "kms:Create*", "kms:Describe*", '
        '"kms:Enable*", "kms:List*", "kms:Put*", "kms:Update*", "kms:ScheduleKeyDeletion", "kms:Revoke*", '
        '"kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource", "kms:UntagResource" ], "Resource" : "*" }]}',
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_kms_key = ret.get("new_state")
    new_target_key_to_update = new_kms_key.get("resource_id")
    # Update alias with test flag
    ret = await hub.states.aws.kms.alias.present(
        test_ctx,
        name=name,
        target_key_id=new_target_key_to_update,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert "Would update aws.kms.alias" in str(ret["comment"])
    resource = ret.get("new_state")
    assert new_target_key_to_update == resource.get("target_key_id")
    # Update alias
    ret = await hub.states.aws.kms.alias.present(
        ctx,
        name=name,
        target_key_id=new_target_key_to_update,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Updated alias of KMS key {new_kms_key.get('resource_id')} to {name}"
        in str(ret["comment"])
    )
    resource = ret.get("new_state")
    assert new_target_key_to_update == resource.get("target_key_id")

    # updating without any changes should not make a call to AWS and simply return a message no changes.
    ret = await hub.states.aws.kms.alias.present(
        ctx,
        name=name,
        target_key_id=new_target_key_to_update,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"KMS Alias '{name}' is already associated with the KMS target key '{new_kms_key.get('resource_id')}'"
        in str(ret["comment"])
    )
    resource = ret.get("new_state")
    assert new_target_key_to_update == resource.get("target_key_id")

    # If we pass an invalid alias name like without prefix alias/ or any invalid alias name
    name = "idem-fixture-alias-" + str(uuid.uuid4())
    ret = await hub.states.aws.kms.alias.present(
        ctx,
        name=name,
        target_key_id=target_key_id,
        resource_id=resource_id,
    )
    assert not ret["result"], ret["comment"]
    assert "(ValidationException)" in str(ret["comment"])

    # Delete alias with test flag
    ret = await hub.states.aws.kms.alias.absent(
        test_ctx,
        name=resource_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert "Would delete aws.kms.alias" in str(ret["comment"])

    # Delete alias
    ret = await hub.states.aws.kms.alias.absent(
        ctx,
        name=resource_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"aws.kms.alias '{resource_id}' is deleted" in str(ret["comment"])

    # delete key
    ret = await hub.states.aws.kms.key.absent(
        ctx, name=key_name, resource_id=new_target_key_to_update
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state")

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.kms.alias.absent(
        ctx,
        name=resource_id,
        resource_id=resource_id,
    )
    assert f"aws.kms.policy '{resource_id}' already absent" in str(ret["comment"])
