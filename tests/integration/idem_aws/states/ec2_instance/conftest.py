import dict_tools.data
import pytest


@pytest.fixture(scope="module", name="instance")
def instance_resource(instance_name):
    resource = dict_tools.data.NamespaceDict(resource_id=None, name=instance_name)
    return resource
