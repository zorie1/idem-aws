import tempfile
from unittest import mock

import pytest
from dict_tools import data

try:
    pass

    HAS_LIBS = True
except ImportError:
    HAS_LIBS = False

ACCT_KEY = "m9MYCrbNve6GO2YN-WfiFgYVaPe3GH7eWF1TL6CI2Qw="
CREDS = {"foo": "bar"}
EXPIRATION = "2020-06-11T12:00:31+0000"


@pytest.fixture(scope="module")
def enc_file():
    with tempfile.NamedTemporaryFile(
        prefix="idem_aws_google_profile", suffix=".fernet", delete=True
    ) as tmp:
        yield tmp.name


@pytest.mark.skipif(not HAS_LIBS, reason="aws_google_auth is not installed")
def test_dump_profile(hub, enc_file):
    with mock.patch.object(
        hub, "OPT", data.NamespaceDict(acct=data.NamespaceDict(acct_key=ACCT_KEY))
    ):
        hub.acct.aws.gsuite.dump_profile(enc_file, CREDS, EXPIRATION)


@pytest.mark.skipif(not HAS_LIBS, reason="aws_google_auth is not installed")
@pytest.mark.asyncio
async def test_load_profile(hub, enc_file):
    with mock.patch.object(
        hub, "OPT", data.NamespaceDict(acct=data.NamespaceDict(acct_key=ACCT_KEY))
    ):
        creds, expiration = await hub.acct.aws.gsuite.load_profile(enc_file)

    assert CREDS == creds
    assert expiration == expiration
