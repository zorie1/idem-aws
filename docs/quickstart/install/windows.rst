Windows
=======


We're working on an installer for Windows, but for now the best way to use Idem
on Windows is to install Python and then pip install Idem.


Install Python
++++++++++++++

.. note::
   Python 3.7 or later required.

To install Idem on Windows, follow these steps.

1. Download and open the most recent `Python for Windows
   <https://www.python.org/downloads/windows/>`_ installer.

#. Select the option to add Python to the Windows PATH environment variable.

   .. image:: ../../_static/img/quickstart/python-windows-installer.png

#. Run the installer, and follow the prompts.

#. In Windows, open **Advanced system settings**, click **Environment
   Variables**, and verify that the Python and PIP executables are included in
   the PATH system variable.

#. Open the Windows command prompt as an Administrator.


Upgrade pip
+++++++++++

#. To meet encryption requirements, upgrade to the latest PIP.

   .. code-block:: bash

    python3 -m pip install --upgrade pip


Install Idem
++++++++++++

#. Install Idem.

   .. code-block:: bash

    pip3 install idem

#. Verify your Idem version.

   .. code-block:: bash

    idem --version

#. Install the `idem-aws` plugin.

   .. code-block:: bash

    pip3 install idem-aws


Upgrade pip
+++++++++++

#. To meet encryption requirements, upgrade to the latest PIP.

   .. code-block:: bash

    python3 -m pip install --upgrade pip


Install Idem
++++++++++++

#. Install Idem.

   .. code-block:: bash

    pip3 install idem

#. Verify your Idem version.

   .. code-block:: bash

    idem --version

#. Install the `idem-aws` plugin.

   .. code-block:: bash

    pip3 install idem-aws


.. include:: ./_includes/install_idem-aws.rst
